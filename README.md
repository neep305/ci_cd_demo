# CI/CD demo

### CI/CD 변수 등록
> Settings > CI/CD에 AWS EC2 IP와 pem파일에 등록된 Certificate 값 등록

### Shell 작성

```shell
#!/bin/bash
#Get servers list
set -f
string=$DEPLOY_SERVER
array=(${string//,/ })
#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"do    
      echo "Deploy project on server ${array[i]}"    
      ssh ubuntu@${array[i]} "cd /var/www && git pull origin master"
done
```
### .gitlab-ci.yml 작성
``` yml
#Production stage
production:   
   stage: production   
   before_script: 
   #generate ssh key   
     - mkdir -p ~/.ssh     
     - echo -e "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa     
     - chmod 600 ~/.ssh/id_rsa     
     - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'   
   script:     
      - bash .gitlab-deploy.sh   
   environment:     
      name: production     
      url: https://social-hits.gsshop.com   
   when: manual
```